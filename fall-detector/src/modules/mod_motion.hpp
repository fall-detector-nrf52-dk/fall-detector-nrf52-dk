#ifndef __MOD_MOTION_H__
#define __MOD_MOTION_H__

#include <Arduino.h>
#include "config.hpp"
#include "MPU9250.h"
#include "util/fifo/fifo.hpp"
#include "util/accel/accel.hpp"
#include "util/logger/logger.hpp"

uint8_t mod_motion_init();
uint8_t mod_motion_read();
uint8_t mod_motion_get_fall_data(accel_t *accel, uint16_t index);

#endif