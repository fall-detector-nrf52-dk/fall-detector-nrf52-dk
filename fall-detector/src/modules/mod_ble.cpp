#include "mod_ble.hpp"

BLEPeripheral fall_detector = BLEPeripheral();
BLEService    fall_service  = BLEService(FALL_SRV_UUID);

BLECharacteristic event_char = BLECharacteristic(FALL_CHR_EVENT_UUID, BLERead | BLEWrite | BLENotify, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
BLECharacteristic coord_char = BLECharacteristic(FALL_CHR_COORD_UUID, BLEWrite, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
// BLECharacteristic latitude__char = BLECharacteristic(FALL_CHR_LATITUDE__UUID, BLEWrite, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
// BLECharacteristic longitude_char = BLECharacteristic(FALL_CHR_LONGITUDE_UUID, BLEWrite, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);

// BLEDescriptor service_desc   = BLEDescriptor("2901", "Fall Detector Service");
// BLEDescriptor event_desc     = BLEDescriptor("2901", "Fall event");
// BLEDescriptor latitude__desc = BLEDescriptor("2901", "Latitude");
// BLEDescriptor longitude_desc = BLEDescriptor("2901", "Longitude");

BLEService    data_service  = BLEService(DATA_SRV_UUID);

BLECharacteristic txctrl_char = BLECharacteristic(DATA_CHR_TXCTRL_UUID, BLERead | BLEWrite | BLENotify, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
BLECharacteristic txbffr_char = BLECharacteristic(DATA_CHR_TXBFFR_UUID, BLERead, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);

geopos_t mod_ble_position;

ble_control_t ble_control;

void mod_ble_init() {
    fall_detector.setLocalName(BLE_LOCAL_NAME);
    fall_detector.setAdvertisedServiceUuid(fall_service.uuid());  

    fall_detector.addAttribute(fall_service);
    fall_detector.addAttribute(event_char);    
    fall_detector.addAttribute(coord_char);
    event_char.setEventHandler(BLEWritten, mod_ble_on_event_written);
    coord_char.setEventHandler(BLEWritten, mod_ble_on_coord_written);

    event_char.setValue(0);    

    fall_detector.addAttribute(data_service);
    fall_detector.addAttribute(txctrl_char);    
    fall_detector.addAttribute(txbffr_char);
    txctrl_char.setEventHandler(BLEWritten, txctrl_on_event_written);

    txctrl_char.setValue(0);

    fall_detector.setEventHandler(BLEConnected, mod_ble_on_connect_handler);
    fall_detector.setEventHandler(BLEDisconnected, mod_ble_on_disconnect_handler);

    fall_detector.begin();

    ble_control.status = BLE_RUNNING;
    ble_control.time_ms = millis();

    geopos_init(&mod_ble_position, 0.0, 0.0, 0);
    logger_info("BLE module ready!");
}

void mod_ble_poll() {
    fall_detector.poll();
}

void mod_ble_end() {
    fall_detector.end();
    ble_control.status = BLE_STAND_BY;

    logger_info("BLE end");
}

void mod_ble_on_connect_handler(BLECentral& central) {
    logger_info("BLE connected");
}

void mod_ble_on_disconnect_handler(BLECentral& central) {
    logger_info("BLE disconnected");
}

void mod_ble_on_event_written(BLECentral& central, BLECharacteristic& characteristic) {
    logger_info("BLE event written");
}

void mod_ble_on_coord_written(BLECentral& central, BLECharacteristic& characteristic) {
    logger_info("BLE coord written");

    // 41.4918886
    //  2.1155701
    // 0xaa4225f7b2400765802a

    if((characteristic.value()[0] == 0xAA) && (characteristic.valueLength() == 10)) {
        if(compute_crc(characteristic.value(), 9) == characteristic.value()[9]){            
            float latitude  = 0.0;
            float longitude = 0.0;
            uint8_t *ptr1, *ptr2; 
            uint32_t temp;            

            temp = 0;
            // temp <<= 8;  // por simetria
            temp |= characteristic.value()[1];
            temp <<= 8;
            temp |= characteristic.value()[2];
            temp <<= 8;
            temp |= characteristic.value()[3];
            temp <<= 8;
            temp |= characteristic.value()[4];

            ptr1 = (uint8_t *)&temp;
            ptr2 = (uint8_t *)&latitude;

            for(uint8_t i = 0; i < sizeof(float); i++) {
                *ptr2++ = *ptr1++;
            }

            temp = 0;
            // temp <<= 8;  // por simetria
            temp |= characteristic.value()[5];
            temp <<= 8;
            temp |= characteristic.value()[6];
            temp <<= 8;
            temp |= characteristic.value()[7];
            temp <<= 8;
            temp |= characteristic.value()[8];

            ptr1 = (uint8_t *)&temp;
            ptr2 = (uint8_t *)&longitude;

            for(uint8_t i = 0; i < sizeof(float); i++) {
                *ptr2++ = *ptr1++;
            }

            char buffer[128];
            sprintf(buffer, "Lat : %.5f Lon : %.5f", latitude, longitude);
            logger_info(buffer);

            if((latitude >= -90.0) && (latitude <= 90.0) && (longitude >= -180.0) && (longitude <= 180.0)) {
                geopos_init(&mod_ble_position, latitude, longitude, millis());
                ble_control.status = BLE_DONE;
            }
        }        
    }    
}

enum {
    DATA_DO_NOTHING,
    DATA_SEND_INDEX
};

union weird {
    uint32_t inumber;
    float_t  fnumber;
};

typedef union weird weird_t;

void txctrl_on_event_written(BLECentral& central, BLECharacteristic& characteristic) {
    logger_critical("Control was written!");

    if(characteristic.value()[0] == DATA_SEND_INDEX) {
        uint8_t buffer[32];
        uint16_t index = (characteristic.value()[1] << 8) | characteristic.value()[2];

        characteristic.setValue(0);
        
        if(index < 10*SAMPLING_FREQUENCY_HZ) {
            accel_t accel;
            weird_t weird;

            mod_motion_get_fall_data(&accel, index);

            weird.fnumber = accel.accel_x;
            buffer[1] = (uint8_t)((weird.inumber >> 24) & 0xFF);
            buffer[2] = (uint8_t)((weird.inumber >> 16) & 0xFF);
            buffer[3] = (uint8_t)((weird.inumber >>  8) & 0xFF);
            buffer[4] = (uint8_t)((weird.inumber      ) & 0xFF);

            weird.fnumber = accel.accel_y;
            buffer[5] = (uint8_t)((weird.inumber >> 24) & 0xFF);
            buffer[6] = (uint8_t)((weird.inumber >> 16) & 0xFF);
            buffer[7] = (uint8_t)((weird.inumber >>  8) & 0xFF);
            buffer[8] = (uint8_t)((weird.inumber      ) & 0xFF);

            weird.fnumber = accel.accel_z;
            buffer[ 9] = (uint8_t)((weird.inumber >> 24) & 0xFF);
            buffer[10] = (uint8_t)((weird.inumber >> 16) & 0xFF);
            buffer[11] = (uint8_t)((weird.inumber >>  8) & 0xFF);
            buffer[12] = (uint8_t)((weird.inumber      ) & 0xFF);

            buffer[0]  = 0xAA;
            buffer[13] = compute_crc(buffer, 13);

            // for(uint8_t i = 0; i < 14; i++) {
            //     Serial.print(buffer[i], HEX);
            //     Serial.print(' ');
            // }

            // Serial.println();

            txbffr_char.setValue((const uint8_t*)buffer, 14);
        }        
    }
}

void mod_ble_run() {
    if(!ble_control.status) {
        mod_ble_init();
    }
    else {
        mod_ble_poll();
    }
}


uint8_t  mod_ble_get_status() {
    return ble_control.status;
}

uint32_t mod_ble_get_age_ms() {
    return millis() - ble_control.time_ms;
}

void mod_ble_get_position(geopos_t *position) {
    position->latitude  = mod_ble_position.latitude;
    position->longitude = mod_ble_position.longitude;
    position->time_ms   = mod_ble_position.time_ms;
}
