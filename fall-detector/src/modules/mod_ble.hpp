#ifndef __MOD_BLE_H__
#define __MOD_BLE_H__

#include <Arduino.h>
#include "config.hpp"
#include <SPI.h>
#include <BLEPeripheral.h>
#include "util/logger/logger.hpp"
#include "util/accel/accel.hpp"
#include "util/crc/crc.hpp"
#include "mod_motion.hpp"
#include "mod_data.hpp"

#define FALL_SRV_UUID                   "83fa0011-8766-4f2f-b7a4-44654f8907e8"
#define FALL_CHR_EVENT_UUID             "83fa0012-8766-4f2f-b7a4-44654f8907e8"
#define FALL_CHR_COORD_UUID             "83fa0013-8766-4f2f-b7a4-44654f8907e8"
// #define FALL_CHR_LATITUDE__UUID         "83fa0003-8766-4f2f-b7a4-44654f8907e8"
// #define FALL_CHR_LONGITUDE_UUID         "83fa0004-8766-4f2f-b7a4-44654f8907e8"

#define DATA_SRV_UUID                   "83fa0021-8766-4f2f-b7a4-44654f8907e8"
#define DATA_CHR_TXCTRL_UUID            "83fa0022-8766-4f2f-b7a4-44654f8907e8"
#define DATA_CHR_TXBFFR_UUID            "83fa0023-8766-4f2f-b7a4-44654f8907e8"

#define BLE_LOCAL_NAME                  "Fall Detector UAB"

enum {
    BLE_STAND_BY=0,
    BLE_RUNNING,
    BLE_DONE
};

struct ble_control_s {    
    uint32_t time_ms;
    uint8_t  status;
};

typedef struct ble_control_s ble_control_t;

uint8_t  mod_ble_get_status();
uint32_t mod_ble_get_age_ms();

void mod_ble_get_position(geopos_t *position);

void mod_ble_init();
void mod_ble_poll();
void mod_ble_run();
void mod_ble_end();

void mod_ble_on_connect_handler(BLECentral& central);
void mod_ble_on_disconnect_handler(BLECentral& central);
void mod_ble_on_event_written(BLECentral& central, BLECharacteristic& characteristic);
void mod_ble_on_coord_written(BLECentral& central, BLECharacteristic& characteristic);
void txctrl_on_event_written(BLECentral& central, BLECharacteristic& characteristic);

#endif