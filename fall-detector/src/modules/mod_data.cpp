#include "mod_data.hpp"

void geopos_init(geopos_t *position, float lat, float lon, float time) {
    position->latitude  = lat;
    position->longitude = lon;
    position->time_ms   = time;    
}

uint32_t geopos_get_age_ms(geopos_t *position) {
    return millis() - position->time_ms;
}