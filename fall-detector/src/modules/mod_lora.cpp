#include "mod_lora.hpp"

lora_control_t lora_control;
void do_send(osjob_t* j);

// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
// static const u1_t PROGMEM APPEUI[8]={ 0x08, 0x49, 0x00, 0xF0, 0x7E, 0xD5, 0xB3, 0x70 };
static const u1_t PROGMEM APPEUI[8]={ 0x2F, 0xE0, 0x02, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// This should also be in little endian format, see above.
// static const u1_t PROGMEM DEVEUI[8]={ 0x32, 0x31, 0x7E, 0xF3, 0x7C, 0x3A, 0xFB, 0x00 };
static const u1_t PROGMEM DEVEUI[8]={ 0x24, 0x75, 0x8E, 0x16, 0x1C, 0xE4, 0xE5, 0x00 };
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
// The key shown here is the semtech default key.
// static const u1_t PROGMEM APPKEY[16] = { 0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C };
static const u1_t PROGMEM APPKEY[16] = { 0x27, 0x8F, 0xAC, 0xC3, 0xBF, 0x45, 0xF6, 0x76, 0xFF, 0xA1, 0xA1, 0x65, 0x5D, 0x42, 0x1B, 0x83 };
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}

// static uint8_t mydata[] = "Almost done!";
static uint8_t mydata[11] = { 0xAA, 0x01, 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04, 0xFF };
static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 60;

// Pin mapping - Arduino
const lmic_pinmap lmic_pins = {
    .nss = 10,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,
    .dio = {2, 3, 4},
};

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));

            // Disable link check validation (automatically enabled
            // during join, but not supported by TTN at this time).
            LMIC_setLinkCheckMode(0);
            break;
        case EV_RFU1:
            Serial.println(F("EV_RFU1"));
            break;
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
            break;
        case EV_TXCOMPLETE:
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.println(F("Received "));
              Serial.println(LMIC.dataLen);
              Serial.println(F(" bytes of payload"));
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
         default:
            Serial.println(F("Unknown event"));
            break;
    }
}

void do_send(osjob_t* j){
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, mydata, sizeof(mydata), 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void mod_lora_init() {
    os_init();    
    LMIC_reset();
    do_send(&sendjob);

    lora_control.status = LORA_RUNNING;
    lora_control.time_ms = millis();

    logger_info("LoRa module ready!");
}

void mod_lora_poll() {
    os_runloop_once();
}

void mod_lora_run() {
    if(!lora_control.status) {
        mod_lora_init();
    }
    else {
        mod_lora_poll();
    }
}

uint8_t  mod_lora_set_data(float lat, float lon, uint8_t event) {
    uint8_t *ptr1, *ptr2; 

    mydata[0] = 0xAA;
    mydata[1] = event;

    ptr1 = (uint8_t *)&lat;
    mydata[5] = *ptr1++;
    mydata[4] = *ptr1++;
    mydata[3] = *ptr1++;
    mydata[2] = *ptr1++;

    ptr1 = (uint8_t *)&lon;
    mydata[9] = *ptr1++;
    mydata[8] = *ptr1++;
    mydata[7] = *ptr1++;
    mydata[6] = *ptr1++;
    
    // ptr1 = (uint8_t *)&lat;
    // ptr2 = (uint8_t *)&mydata[2];

    // for(uint8_t i = 0; i < sizeof(float); i++) {
    //     *ptr2++ = *ptr1++;
    // }

    // ptr1 = (uint8_t *)&lon;
    // ptr2 = (uint8_t *)&mydata[6];

    // for(uint8_t i = 0; i < sizeof(float); i++) {
    //     *ptr2++ = *ptr1++;
    // }

    mydata[10] = compute_crc(mydata, 10);

    // for(uint8_t i = 0; i < 11; i++) {
    //     Serial.print(mydata[i], HEX);
    //     Serial.print(' ');
    // }
    
    // Serial.println();
}

uint8_t  mod_lora_get_status() {
    return lora_control.status;
}

uint32_t mod_lora_get_age_ms() {
    return millis() - lora_control.time_ms;
}

