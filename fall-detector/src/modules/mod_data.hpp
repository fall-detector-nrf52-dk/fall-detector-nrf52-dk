#ifndef __MOD_DATA_H__
#define __MOD_DATA_H__

#include <Arduino.h>

struct geopos_s {
    float latitude;
    float longitude;
    uint32_t time_ms;
};

typedef struct geopos_s geopos_t;

void geopos_init(geopos_t *position, float lat, float lon, float time);
uint32_t geopos_get_age_ms(geopos_t *position);

#endif