#ifndef __MOD_LORA_H__
#define __MOD_LORA_H__

#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>

#include "util/logger/logger.hpp"
#include "util/crc/crc.hpp"

enum {
    LORA_STAND_BY=0,
    LORA_RUNNING,
    LORA_DONE
};

struct lora_control_s {    
    uint32_t time_ms;
    uint8_t  status;
};

typedef struct lora_control_s lora_control_t;

uint8_t  mod_lora_set_data(float lat, float lon, uint8_t event);
uint8_t  mod_lora_get_status();
uint32_t mod_lora_get_age_ms();

void mod_lora_run();

#endif