#include "mod_motion.hpp"

MPU9250 IMU(Wire,0x68);
int status;

fifo_t fifo;

// fall vector
accel_t fall[10*SAMPLING_FREQUENCY_HZ];

// contador para los ultimos 5 segundos
int counter = 0;

uint8_t mod_motion_init() {
    // start communication with IMU 
    status = IMU.begin();
    if (status < 0) {
        logger_critical("Could not initialize MPU9250.");
        logger_critical("End of the program...");
        while(1) {}
    }
    // setting the accelerometer full scale range to +/-8G 
    IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
    // setting the gyroscope full scale range to +/-500 deg/s
    IMU.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
    // setting DLPF bandwidth to 20 Hz
    IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_184HZ);
    // setting SRD to 19 for a 50 Hz update rate
    IMU.setSrd(19);

    fifo_init(&fifo, 10*SAMPLING_FREQUENCY_HZ, sizeof(accel_t));

    logger_info("Motion module ready!");

    return 0;
}

uint8_t mod_motion_analyze(accel_t *accel) {
    uint8_t event = 0;

    logger_info("Analyzing falling data...");

    // 1er criterio: mayor que 2.5 G en medio
    for(uint32_t i = 4*SAMPLING_FREQUENCY_HZ; i < 6*SAMPLING_FREQUENCY_HZ; i++) {
        if(accel_get_magnitude(&fall[i]) >= ACCEL_HIGH_THRESHOLD) {
            event <<= 1;
            event |= 1;
            logger_info("1st criteria mag >= 2.5 G detected.");
            break;
        }
    }

    // 2do criterio: angulo ~ 90
    float angle = accel_get_angle(&fall[3*SAMPLING_FREQUENCY_HZ], &fall[8*SAMPLING_FREQUENCY_HZ]);

    if((angle >= ANGLE_LOWER_LIMIT) && (angle <= ANGLE_UPPER_LIMIT)) {        
        event <<= 1;
        event |= 1;
        logger_info("2nd criteria ang ~ 90.0 degrees detected.");
    }

    // 3er criterio: menor que 0.5 G antes de medio (opcional)
    for(uint32_t i = 4*SAMPLING_FREQUENCY_HZ; i < 5*SAMPLING_FREQUENCY_HZ; i++) {
        if(accel_get_magnitude(&fall[i]) <= ACCEL_LOW_THRESHOLD) {
            event <<= 1;
            event |= 1;
            logger_info("3rd criteria mag <= 0.5 G before max detected.");
            break;
        }
    }

    return event;
}

uint8_t mod_motion_read() {
    accel_t accel;
    uint8_t event = 0;

    IMU.readSensor();
    accel_init(&accel, IMU.getAccelX_mss(), IMU.getAccelY_mss(), IMU.getAccelZ_mss());
    fifo_put_element(&fifo, &accel);

    if(accel_get_magnitude(&accel) >= ACCEL_HIGH_THRESHOLD) {
        counter = 5*SAMPLING_FREQUENCY_HZ;

        logger_debug("High magnitude event detected!");
    }

    if(counter > 0) {
        if(--counter == 0) {
            accel_t temp;

            for(uint32_t i = 0; i < 10*SAMPLING_FREQUENCY_HZ; i++) {
                fifo_get_element(&fifo, &temp);
                accel_copy(&fall[i], &temp);                
            }

            // for(uint32_t i = 0; i < 10*SAMPLING_FREQUENCY_HZ; i++) {
            //     accel_print(&fall[i]);
            // }

            if(mod_motion_analyze(fall) >= 0b00000110) {
                event = 1;

                logger_critical("Falling event detected!");                
            }
        }
    }

    return event;
}

uint8_t mod_motion_get_fall_data(accel_t *accel, uint16_t index) {
    if(index < 10*SAMPLING_FREQUENCY_HZ) {
        accel->accel_x = fall[index].accel_x;
        accel->accel_y = fall[index].accel_y;
        accel->accel_z = fall[index].accel_z;

        return 1;
    }

    return 0;
}
