#include <Arduino.h>
#include "config.hpp"
#include "util/logger/logger.hpp"
#include "modules/mod_motion.hpp"
#include "modules/mod_data.hpp"
#include "modules/mod_lora.hpp"
#include "modules/mod_ble.hpp"

void main_task();
void emul_evnt();

void led_init();
void led_toggle();

geopos_t position;
uint32_t last_millis = 0;

uint8_t event   = 0;
uint8_t process = 0;

uint8_t process_ble  = 0;
uint8_t process_lora = 0;

void setup() {    
    Serial.begin(115200);
    // logging throug serial port
    logger_init(LOGGING_LEVEL);
    // 
    mod_motion_init();
    // initial position of system
    geopos_init(&position, DEFAULT_LATITUDE, DEFAULT_LONGITUDE, millis());

    led_init();
    // system starts operating
    logger_info("Bravo Six going dark...");
}

void loop() {    
    main_task();

    if(!process) {
        // low power
    }
}


// main task
void main_task() {
    emul_evnt();

    // every x ms updates position
    if(mod_ble_get_age_ms() >= TIME_BETWEEN_SYNCHRO_MS) {
        event = EVENT_SYNCHRO;
    }

    if(event) {
        process_ble = event;
        event = EVENT_NONE;
    }

    if(process_ble) {
        mod_ble_run();

        // if position has been received
        if(mod_ble_get_status() == BLE_DONE) {
            geopos_t temp;
            mod_ble_get_position(&temp);

            // if position data is newer than the one already in vars
            if(geopos_get_age_ms(&temp) < geopos_get_age_ms(&position)) {
                position.latitude  = temp.latitude;
                position.longitude = temp.longitude;
                position.time_ms   = temp.time_ms;
            }

            // if event is a fall or a panic button
            if((process_ble == EVENT_FALL) || (process_ble == EVENT_BUTTON)) {
                mod_lora_set_data(position.latitude, position.longitude, process_ble);
                process_lora = 1;
            }

            // ble process done
            process_ble = 0;
            mod_ble_end();
        }

        // if ble has been advertising for a minute or more, use last coordinates
        if(mod_ble_get_age_ms() >= BLE_POSITION_TIMEOUT_MS) {
            if((process_ble == EVENT_FALL) || (process_ble == EVENT_BUTTON)) {
                mod_lora_set_data(position.latitude, position.longitude, process_ble);
                process_lora = 1;
            }  
            process_ble = 0;
            mod_ble_end();
        }
    }

    if(process_lora) {
        mod_lora_run();

        // if(mod_lora_get_age_ms() >= 300000) {
        //     process_lora = 0;
        // }
    }

    // every x ms
    if((millis() - last_millis) > TIME_BETWEEN_SAMPLES_MS) {
        last_millis = millis();

        event = mod_motion_read();

        if(process) {
            led_toggle();
        }
    }
    
    process = process_ble | process_lora;
}

// function for led
void led_init() {
    pinMode(PIN_LED2, OUTPUT);
    digitalWrite(PIN_LED2, HIGH);
}

void led_toggle() {
    static uint8_t counter = 0;
    static uint8_t status = 0;

    if(++counter >= 5) {
        counter = 0;

        if(status) {
            status = 0;
            digitalWrite(PIN_LED2, LOW);
        }
        else {
            status = 1;
            digitalWrite(PIN_LED2, HIGH);
        }        
    }
}

// function for emulating events from serial
void emul_evnt() {
    if(Serial.available()) {
        uint8_t temp = Serial.read();

        if(temp == 'x') {
            event = EVENT_FALL;
        }

        if(temp == 'y') {
            event = EVENT_BUTTON;
        }

        if(temp == 'z') {
            event = EVENT_SYNCHRO;
        }
    }
}