#ifndef __FIFO_H__
#define __FIFO_H__

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef struct fifo_struct
{
    void *buffer;
    void *read_ptr;
    void *write_ptr;

    uint8_t length_of_element;
    uint32_t length;

    uint32_t elements;
} fifo_t;

void fifo_init(fifo_t *_fifo, uint32_t _length, uint8_t _length_of_element);
void fifo_deinit(fifo_t *_fifo);

void fifo_clean(fifo_t *_fifo);

void fifo_put_element(fifo_t *_fifo, void *_data);
void fifo_put_elements(fifo_t *_fifo, void *_data, uint32_t _data_length);

uint32_t fifo_get_number_of_elements(fifo_t *_fifo);
void fifo_get_element(fifo_t *_fifo, void *_data);
void fifo_get_elements(fifo_t *_fifo, void *_data, uint32_t _data_length);

#endif
