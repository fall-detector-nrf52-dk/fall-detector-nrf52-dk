#include "fifo.hpp"

void fifo_init(fifo_t *_fifo, uint32_t _length, uint8_t _length_of_element)
{
    _fifo->length = _length;
    _fifo->length_of_element = _length_of_element;
    _fifo->elements = 0;
    _fifo->buffer = malloc(_fifo->length * _fifo->length_of_element);
    _fifo->read_ptr = _fifo->buffer;
    _fifo->write_ptr = _fifo->buffer;
}

void fifo_deinit(fifo_t *_fifo)
{
    _fifo->length = 0;
    _fifo->length_of_element = 0;
    _fifo->elements = 0;

    free(_fifo->buffer);
    _fifo->read_ptr = 0;
    _fifo->write_ptr = 0;
}

void fifo_clean(fifo_t *_fifo)
{
    _fifo->elements = 0;
    _fifo->read_ptr = _fifo->buffer;
    _fifo->write_ptr = _fifo->buffer;
}

void fifo_put_element(fifo_t *_fifo, void *_data)
{
    memcpy(_fifo->write_ptr, _data, _fifo->length_of_element);

    _fifo->write_ptr += _fifo->length_of_element;

    if (_fifo->write_ptr >= _fifo->buffer + (_fifo->length * _fifo->length_of_element))
        _fifo->write_ptr = _fifo->buffer;

    // si la fifo se llena, destruye el dato mas viejo (actualiza el puntero de lectura)
    if (_fifo->write_ptr == _fifo->read_ptr) {
        _fifo->read_ptr += _fifo->length_of_element;

        if (_fifo->read_ptr >= _fifo->buffer + (_fifo->length * _fifo->length_of_element))
            _fifo->read_ptr = _fifo->buffer;
    }        

    if (_fifo->elements < _fifo->length)
        ++_fifo->elements;
}

void fifo_put_elements(fifo_t *_fifo, void *_data, uint32_t _data_length)
{
    for (uint32_t i = 0; i < _data_length; ++i)
        fifo_put_element(_fifo, _data + i * _fifo->length_of_element);
}

uint32_t fifo_get_number_of_elements(fifo_t *_fifo)
{
    return _fifo->elements;
}

void fifo_get_element(fifo_t *_fifo, void *_data)
{
    memcpy(_data, _fifo->read_ptr, _fifo->length_of_element);

    _fifo->read_ptr += _fifo->length_of_element;

    if (_fifo->read_ptr >= _fifo->buffer + (_fifo->length * _fifo->length_of_element))
        _fifo->read_ptr = _fifo->buffer;

    if (_fifo->elements > 0)
        --_fifo->elements;
}

void fifo_get_elements(fifo_t *_fifo, void *_data, uint32_t _data_length)
{
    for (uint32_t i = 0; i < _data_length; ++i)
        fifo_get_element(_fifo, _data + i * _fifo->length_of_element);
}
