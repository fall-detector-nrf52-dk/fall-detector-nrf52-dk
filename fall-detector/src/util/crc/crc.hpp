#ifndef __CRC_H__
#define __CRC_H__

#include <Arduino.h>

uint8_t compute_crc(const uint8_t *buffer, uint8_t size);

#endif