#include "crc.hpp"

uint8_t compute_crc(const uint8_t *buffer, uint8_t size) {
    uint8_t crc = 0;

    for(uint8_t i = 0; i < size; i++) {
        crc ^= buffer[i];
    }

    return crc;
}