#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <Arduino.h>

#define LOGGING_OFF                     0
#define LOGGING_CRITICAL                10
#define LOGGING_ERROR                   20
#define LOGGING_WARNING                 30
#define LOGGING_INFO                    40
#define LOGGING_DEBUG                   50

void logger_init(uint8_t level);

void logger_critical(const char* msg);
void logger_error   (const char* msg);
void logger_warning (const char* msg);
void logger_info    (const char* msg);
void logger_debug   (const char* msg);

#endif