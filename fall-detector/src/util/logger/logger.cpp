#include "logger.hpp"

static uint8_t logging_level = LOGGING_OFF;

void logger_init(uint8_t level) {
    logging_level = level;
}

void logger_critical(const char* msg) {
    if(logging_level >= LOGGING_CRITICAL) {
        char buffer[128];

        sprintf(buffer, "%10lu ms : CRITICAL : %s\n", millis(), msg);
        Serial.print(buffer);
    }
}

void logger_error   (const char* msg) {
    if(logging_level >= LOGGING_ERROR) {
        char buffer[128];

        sprintf(buffer, "%10lu ms : ERROR : %s\n", millis(), msg);
        Serial.print(buffer);
    }
}

void logger_warning (const char* msg) {
    if(logging_level >= LOGGING_WARNING) {
        char buffer[128];

        sprintf(buffer, "%10lu ms : WARNING : %s\n", millis(), msg);
        Serial.print(buffer);
    }
}

void logger_info    (const char* msg) {
    if(logging_level >= LOGGING_INFO) {
        char buffer[128];

        sprintf(buffer, "%10lu ms : INFO : %s\n", millis(), msg);
        Serial.print(buffer);
    }
}

void logger_debug   (const char* msg) {
    if(logging_level >= LOGGING_DEBUG) {
        char buffer[128];

        sprintf(buffer, "%10lu ms : DEBUG : %s\n", millis(), msg);
        Serial.print(buffer);
    }
}