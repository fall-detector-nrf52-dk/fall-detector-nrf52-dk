#ifndef __ACCEL_H__
#define __ACCEL_H__

#include <Arduino.h>
#define ACCEL_CTE_G_MSS                 9.80

struct accel_s {
    float accel_x;
    float accel_y;
    float accel_z;
};

typedef struct accel_s accel_t;

void accel_init(accel_t *accel, float ax, float ay, float az);
float accel_get_magnitude(accel_t *accel);
float accel_get_angle(accel_t *accel1, accel_t *accel2);
void accel_copy(accel_t *dst, accel_t *src);
void accel_print(accel_t *accel);

#endif