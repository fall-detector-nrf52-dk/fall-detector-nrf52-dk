#include "accel.hpp"

void accel_init(accel_t *accel, float ax, float ay, float az) {
    accel->accel_x = ax / ACCEL_CTE_G_MSS;
    accel->accel_y = ay / ACCEL_CTE_G_MSS;
    accel->accel_z = az / ACCEL_CTE_G_MSS;
}

float accel_get_magnitude(accel_t *accel) {
    return sqrtf(accel->accel_x*accel->accel_x + 
                 accel->accel_y*accel->accel_y + 
                 accel->accel_z*accel->accel_z);
}

float _dot_product(accel_t *accel1, accel_t *accel2) {
    return accel1->accel_x*accel2->accel_x + accel1->accel_y*accel2->accel_y + accel1->accel_z*accel2->accel_z;
}

float accel_get_angle(accel_t *accel1, accel_t *accel2) {
    float angle = 0.0;
    float _dot  = 0.0;
    float _mag  = 1.0;

    _dot = _dot_product(accel1, accel2);
    _mag = accel_get_magnitude(accel1) * accel_get_magnitude(accel1);

    if(_mag > 0.0) {
        angle = acos(_dot / _mag) * 180.0 / PI;;
    }

    return angle;
}

void accel_copy(accel_t *dst, accel_t *src) {
    dst->accel_x = src->accel_x;
    dst->accel_y = src->accel_y;
    dst->accel_z = src->accel_z;
}

void accel_print(accel_t *accel) {
    static float max_accel =  0.0;
    static float min_accel = 10.0;
    float mag_accel;
    char  buffer[256];

    mag_accel = accel_get_magnitude(accel);

    max_accel = max(max_accel, mag_accel);
    min_accel = min(min_accel, mag_accel);

    sprintf(buffer, "ax: %7.5f\t ay: %7.5f\t az: %7.5f \t ma: %7.5f\t mx: %7.5f\t mn: %7.5f\r\n", 
        accel->accel_x, accel->accel_y, accel->accel_z, mag_accel, max_accel, min_accel);

    Serial.print(buffer);
}

