#ifndef __CONFIG_H__
#define __CONFIG_H__

#define DEFAULT_LATITUDE                41.3870814
#define DEFAULT_LONGITUDE                2.1699678

#define BLE_POSITION_TIMEOUT_MS         60000 // 1 minute

#define SAMPLING_FREQUENCY_HZ           50
#define TIME_BETWEEN_SAMPLES_MS         1000 / SAMPLING_FREQUENCY_HZ

#define TIME_BETWEEN_SYNCHRO_MS         3600000 // 1 hour
#define LOGGING_LEVEL                   LOGGING_DEBUG

#define ACCEL_HIGH_THRESHOLD            2.5 // 2.5 G
#define ACCEL_LOW_THRESHOLD             0.5 // 0.5 G

#define ANGLE_UPPER_LIMIT               105.0 // degrees
#define ANGLE_LOWER_LIMIT               75.0  // degrees


enum event_e {
    EVENT_NONE,
    EVENT_FALL,
    EVENT_BUTTON,
    EVENT_SYNCHRO
};

#endif