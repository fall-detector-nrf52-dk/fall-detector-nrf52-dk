// #include "drv_ble.hpp"

// BLEPeripheral fall_detector = BLEPeripheral();
// BLEService    fall_service  = BLEService(FALL_SRV_UUID);

// BLECharacteristic event_char = BLECharacteristic(FALL_CHR_EVENT_UUID, BLERead | BLEWrite | BLENotify, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
// BLECharacteristic coord_char = BLECharacteristic(FALL_CHR_COORD_UUID, BLEWrite, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
// // BLECharacteristic latitude__char = BLECharacteristic(FALL_CHR_LATITUDE__UUID, BLEWrite, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
// // BLECharacteristic longitude_char = BLECharacteristic(FALL_CHR_LONGITUDE_UUID, BLEWrite, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);

// // BLEDescriptor service_desc   = BLEDescriptor("2901", "Fall Detector Service");
// // BLEDescriptor event_desc     = BLEDescriptor("2901", "Fall event");
// // BLEDescriptor latitude__desc = BLEDescriptor("2901", "Latitude");
// // BLEDescriptor longitude_desc = BLEDescriptor("2901", "Longitude");

// void fall_detector_ble_init() {
//     fall_detector.setLocalName(BLE_LOCAL_NAME);
//     fall_detector.setAdvertisedServiceUuid(fall_service.uuid());  

//     fall_detector.addAttribute(fall_service);
//     // fall_detector.addAttribute(service_desc);    

//     fall_detector.addAttribute(event_char);
//     // fall_detector.addAttribute(event_desc);
//     fall_detector.addAttribute(coord_char);

//     fall_detector.setEventHandler(BLEConnected, fall_detector_ble_on_connect_handler);
//     fall_detector.setEventHandler(BLEDisconnected, fall_detector_ble_on_disconnect_handler);
//     event_char.setEventHandler(BLEWritten, fall_detector_ble_on_event_written);
//     coord_char.setEventHandler(BLEWritten, fall_detector_ble_on_coord_written);

//     event_char.setValue(0);

//     fall_detector.begin();
// }

// void fall_detector_ble_poll() {
//     fall_detector.poll();
// }

// void fall_detector_ble_on_connect_handler(BLECentral& central) {
//     Serial.println("Connected");
// }

// void fall_detector_ble_on_disconnect_handler(BLECentral& central) {
//     Serial.println("Disconnected");
// }

// void fall_detector_ble_on_event_written(BLECentral& central, BLECharacteristic& characteristic) {
//     Serial.println("Event written");
// }

// void fall_detector_ble_on_coord_written(BLECentral& central, BLECharacteristic& characteristic) {
//     Serial.println("Coord written");
// }