// #ifndef __DRV_BLE_H__
// #define __DRV_BLE_H__

// #include <Arduino.h>
// #include <SPI.h>
// #include <BLEPeripheral.h>

// #define FALL_SRV_UUID                   "83fa0001-8766-4f2f-b7a4-44654f8907e8"
// #define FALL_CHR_EVENT_UUID             "83fa0002-8766-4f2f-b7a4-44654f8907e8"
// #define FALL_CHR_COORD_UUID             "83fa0003-8766-4f2f-b7a4-44654f8907e8"
// // #define FALL_CHR_LATITUDE__UUID         "83fa0003-8766-4f2f-b7a4-44654f8907e8"
// // #define FALL_CHR_LONGITUDE_UUID         "83fa0004-8766-4f2f-b7a4-44654f8907e8"

// #define BLE_LOCAL_NAME                  "Fall Detector"

// void fall_detector_ble_init();
// void fall_detector_ble_poll();

// void fall_detector_ble_on_connect_handler(BLECentral& central);
// void fall_detector_ble_on_disconnect_handler(BLECentral& central);
// void fall_detector_ble_on_event_written(BLECentral& central, BLECharacteristic& characteristic);
// void fall_detector_ble_on_coord_written(BLECentral& central, BLECharacteristic& characteristic);

// #endif