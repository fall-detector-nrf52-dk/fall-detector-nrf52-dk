#ifndef __SPI_H__
#define __SPI_H__

#include <Arduino.h>
#include "SPI.h"

#define SPI_WRITE_MASK                  0x80
#define SPI_READ_MASK                   0x7F

enum { DONE_SUCCESS, DONE_ERROR };

uint8_t spi_init();
// uint8_t spi_write(uint8_t address, uint8_t data);
// uint8_t spi_read(uint8_t address, uint8_t* data);

void spi_write(uint8_t address, uint8_t data);
uint8_t spi_read(uint8_t address);

void spi_write_burst(uint8_t address, uint8_t* buffer, uint8_t nbytes);
void spi_read_burst(uint8_t address, uint8_t* buffer, uint8_t nbytes);

void write_fifo(uint8_t* buffer, uint8_t size);
void read_fifo(uint8_t* buffer, uint8_t size);

#endif