#include "spi.hpp"

uint8_t spi_init() {
    SPI.begin();
    SPI.setBitOrder(MSBFIRST);
    SPI.setClockDivider(SPI_CLOCK_DIV64);
    SPI.setDataMode(SPI_MODE0);
    delayMicroseconds(100);

    pinMode(SS, OUTPUT);
    digitalWrite(SS, HIGH);
    delayMicroseconds(100);
}

// uint8_t spi_write(uint8_t address, uint8_t data) {
//     digitalWrite(SS, LOW);
//     // address |= SPI_WRITE_MASK;
//     bitSet(address, 7);
//     SPI.transfer(address);
//     SPI.transfer(data);
//     digitalWrite(SS, HIGH);

//     return DONE_SUCCESS;
// }

// uint8_t spi_read(uint8_t address, uint8_t* data) {
//     digitalWrite(SS, LOW);
//     address &= SPI_READ_MASK;
//     SPI.transfer(address);
//     *data = SPI.transfer(0x00);
//     digitalWrite(SS, HIGH);

//     return DONE_SUCCESS;
// }

void spi_write(uint8_t address, uint8_t data) {
    digitalWrite(SS, LOW);
    // address |= SPI_WRITE_MASK;
    bitSet(address, 7);
    SPI.transfer(address);
    SPI.transfer(data);
    digitalWrite(SS, HIGH);
}

uint8_t spi_read(uint8_t address) {
    uint8_t data;
    digitalWrite(SS, LOW);
    address &= SPI_READ_MASK;
    SPI.transfer(address);
    data = SPI.transfer(0x00);
    digitalWrite(SS, HIGH);

    return data;
}

void spi_write_burst(uint8_t address, uint8_t* buffer, uint8_t nbytes) {
    digitalWrite(SS, LOW);
    address |= SPI_WRITE_MASK;
    SPI.transfer(address);
    for(uint8_t i = 0; i < nbytes; i++) {
        SPI.transfer(buffer[i]);
    }
    digitalWrite(SS, HIGH);
}

void spi_read_burst(uint8_t address, uint8_t* buffer, uint8_t nbytes) {
    digitalWrite(SS, LOW);
    address &= SPI_READ_MASK;
    SPI.transfer(address);
    for(uint8_t i = 0; i < nbytes; i++) {
        buffer[i] = SPI.transfer(0x00);
    }
    digitalWrite(SS, HIGH);
}

void write_fifo(uint8_t* buffer, uint8_t size) {
    spi_write_burst(0, buffer, size);
}

void read_fifo(uint8_t* buffer, uint8_t size) {
    spi_read_burst(0, buffer, size);
}